# Bleacher

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

  IEx 1.9.4 (compiled with Erlang/OTP 21)

## Working with the project
  1. Run test to make sure the core data is being transformed in the required manner.

 ```
   mix test
 ``
  NOTE: Only the content module is tested.

2. To test the endpoints use post man or curl.

3. Add data 

  url: http://localhost:4000/api/reaction
   request: POST
   body: 
    {
	    "type": "reaction",
	    "action": "add",
	    "content_id": "23",
	    "user_id": "3",
	    "reaction_type": "fire"
    }


  


4. Remove data

 url: http://localhost:4000/api/reaction
   request: POST
   body: 
    {
	    "type": "reaction",
	    "action": "remove",
	    "content_id": "23",
	    "user_id": "3",
	    "reaction_type": "fire"
    }

    response: success

 * create a new topic if the content_id is null(if unfiring from unavailable content) and add the user to unfire List.
 * if content_id is true add the user to unfire List and remove the user from the particular content.

 5. Show content.

 url: http://localhost:4000/api/reaction_counts/:content_id

 * returns an error json with status code 404 else the approriate response with 200 status code.


 6. Testing Genserver crashing code.
  Since the project had an issue with running :observer.start to kill the genserver instance, we are using the terminate function in the cache module.
  ie
  ```
  iex> iex -S mix phx.server
  iex> Bleacher.Cache.terminate
  ```


## Assumptions

The request come only from the application hence no security checks implemented.

## Minors
The project was created without ecto and this brought an issue with running tools like :observer.start.