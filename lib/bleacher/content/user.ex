defmodule Bleacher.Content.User do
  defstruct id: nil

  alias __MODULE__

  def new(id), do: %User{id: id}

  def add_update_users([], %User{} = user), do: [user]

  def add_update_users(users, %User{} = user) do
    # steps
    # 1st check if user exits
    # 2nd, if yes, do nothing
    # 3nd, if no add a new user
    case  Enum.filter(users, &does_user_exist(&1, user)) do
      [] ->
        [user | users]
     _->
        users
    end

  end


  def remove_update_users(users, %User{} = user) do
    # remove if user is in this list
    Enum.reject(users, &does_user_exist(&1, user))
  end


  def count(users), do: Enum.count(users)

  defp does_user_exist(user, new_user), do: user.id == new_user.id
end
