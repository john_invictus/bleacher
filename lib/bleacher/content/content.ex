defmodule Bleacher.Content do
  @moduledoc ~S"""
    This module will have a sole role of manupulating content data provided to it.
  """

  alias __MODULE__.User
  alias __MODULE__

  defstruct id: nil, count: 0, fires: [], unfire: []

  @doc """
   create a new content when given the ID
  """
  def new(id), do: %Content{id: id}

  @doc """
   create a new content when given content and valid user
  """
  def new(content, %User{} = user),
      do: %{content | fires: User.add_update_users(content.fires, user)}


  def new(content, nil), do: content

  @doc ~S"""
    This function will be used to update or add new content to list.
  """
  def add_update_contents(contents, %Content{} = content, %User{} = user)  do

    value =
      contents
      |> Enum.filter(&does_content_exist(&1, content.id))

    case value do
       [] ->
        [new(content, user) | contents]

      [value] ->
        new_list =
          contents
          |> Enum.reject(&does_content_exist(&1, content.id))

        updated_content = %{value | fires: User.add_update_users(value.fires, user)} |> remove_unfire_user(user)

        [updated_content | new_list]

        _->
          contents

    end
  end

  @doc ~S"""
   This function is used to remove a user from topic, unfire
   if no topic available return no change
  """
  def remove_update_contents(contents, %Content{} = content, %User{} = user) do
      case Enum.filter(contents, &does_content_exist(&1, content.id)) do
        [] ->
          new_content = new(content, nil) |> add_unfire_user(user)
          [new_content | contents]

        [value] ->
          new_list =
            contents
            |> Enum.reject(&does_content_exist(&1, content.id))

            updated_content = %{value | fires: User.remove_update_users(value.fires, user)} |> add_unfire_user(user)

            [updated_content | new_list]
      end
  end


  @doc """
   get content when give a content ID
  """
  @spec get_content(List.t(), String.t()) :: nil | String.t()
  def get_content(contents, content_id) do
    Enum.filter(contents, &does_content_exist(&1, content_id))
     |> case do
      [] -> nil

      [value] ->
        %{value | count: User.count(value.fires)}

    end
  end

  @doc """
    This function makes sure that the user who is added to fire is not in unfired list
  """
  def remove_unfire_user(%Content{unfire: unfire} = content, %User{id: user_id}) do
    updated_unfire =
      unfire
      |> Enum.reject(fn u -> u.id == user_id end)

    %{content | unfire: updated_unfire}
  end


   @doc """
   This function will add users to unfires list
  """
  def add_unfire_user(%Content{unfire: unfire} = content, %User{id: user_id} = user) do
      case Enum.filter(unfire, fn u -> u.id == user_id end) do
        [] ->
          %{content| unfire: [user| unfire]}

        _ ->
          content
      end
  end


  @doc """
    This function will map the current content and add count to it
  """
  def add_count(contents) do
    contents
    |> Enum.map(fn content -> %{content | count: User.count(content.fires) } end)
  end


  defp does_content_exist(%Content{} = content, content_id),
      do: content.id == content_id

end
