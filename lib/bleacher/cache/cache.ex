defmodule Bleacher.Cache do
  use GenServer

  alias Bleacher.Content.User
  alias Bleacher.Content


  @content_cache ~S(contents)


  @moduledoc """
    It will be used to maintain in memory cache
  """

  @name __MODULE__

  ################################ Public API
  def start_link(_) do
    GenServer.start_link(@name, :ok, name: @name)
  end


  def add_action(content_id, user_id) do
    GenServer.cast(@name, {:add, Content.new(content_id), User.new(user_id)})
  end


  def remove_action(content_id, user_id) do
    GenServer.cast(@name, {:remove, Content.new(content_id), User.new(user_id)})
  end

    @doc """
  kill genserver just to test the ets caching
  """
  def terminate do
    GenServer.cast(@name, :stop)
  end

  def get_content(content_id) do
    GenServer.call(@name, {:get, content_id})
  end

  def get_all_content do
    GenServer.call(@name, :all)
  end


  ############################### PRIVATE API

  @impl true
  def init(:ok) do

    case :ets.lookup(:cache_table, @content_cache) do
      [] ->
        {:ok, []}

      [{@content_cache, contents}] ->
        {:ok, contents}
    end

    {:ok, []}

  end

  @impl true
  def handle_cast({:add, content, user}, state) do
    updated_contents = Content.add_update_contents(state, content, user)

    # add data to ets
    :ets.insert(:cache_table, {@content_cache, updated_contents})


    {:noreply, updated_contents}
  end


  @impl true
  def handle_cast({:remove, content, user}, state) do
      updated_contents = Content.remove_update_contents(state, content, user)

    # add data to ets
    :ets.insert(:cache_table, {@content_cache, updated_contents})

    {:noreply, updated_contents}
  end

  @impl true
  def handle_cast(:stop, state) do
    {:stop, :normal, state}
  end



  @impl true
  def handle_call({:get, content_id}, _from, state) do
    {:reply, Content.get_content(state, content_id), state}
  end

  @impl true
  def handle_call(:all, _from, state) do
    {:reply, Content.add_count(state), state}
  end
end
