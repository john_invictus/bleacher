defmodule BleacherWeb.Router do
  use BleacherWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BleacherWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  #Other scopes may use custom stacks.
  scope "/api", BleacherWeb do
    pipe_through :api

    get "/reaction", CachingController, :index # This router will list all the available cached contents

    post "/reaction", CachingController, :create
    get "/reaction_counts/:content_id", CachingController, :show, param: "content_id"
  end
end
