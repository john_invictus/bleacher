defmodule BleacherWeb.CachingView do
  use BleacherWeb, :view

  alias Bleacher.Content
  alias Bleacher.Content.User

  alias __MODULE__

  def render("index.json", %{contents: contents}) do
    %{contents: render_many(contents, CachingView, "content.json", as: :content)}
  end

  def render("success.json", %{message: message}) do
    %{message: message}
  end

  def render("show.json", %{content: content}) do
    %{content: render_one(content, CachingView, "content.json", as: :content)}
  end

  def render("content.json", %{content: content}) do
    %{
      content_id: content.id,
      reaction_count: %{
        fire: content.count
      }
     }
  end

  def render("user.json", %{user: user}) do
    IO.inspect user
   %{id: user.id}
  end
end
