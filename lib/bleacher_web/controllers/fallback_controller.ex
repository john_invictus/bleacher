defmodule BleacherWeb.FallbackController do
  use BleacherWeb, :controller

  def call(conn, {:error, :not_found, message}) do
    conn
    |> put_status(:not_found)
    |> render(BleacherWeb.ErrorView, "error.json", message: message)
  end

  def call(conn, {:error, :invalid_inputs, message}) do
    conn
    |> put_status(:bad_request)
    |> render(BleacherWeb.ErrorView, "error.json", message: message)
  end

end
