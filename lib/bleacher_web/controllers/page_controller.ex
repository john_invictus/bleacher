defmodule BleacherWeb.PageController do
  use BleacherWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
