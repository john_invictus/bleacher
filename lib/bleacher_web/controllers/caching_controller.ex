defmodule BleacherWeb.CachingController do
  use BleacherWeb, :controller

  alias Bleacher.Cache

  action_fallback(BleacherWeb.FallbackController)


  def index(conn, _params) do
    contents = Cache.get_all_content()
    IO.inspect contents


    render(conn, "index.json", contents: contents)
  end


  def create(conn, %{"type" => "reaction", "action" => "add",
   "content_id" => content_id, "user_id" => user_id, "reaction_type" => "fire" }) do
    Cache.add_action(content_id, user_id)

    render(conn, "success.json", message: "success")
   end

   def create(conn, %{"type" => "reaction", "action" => "remove",
   "content_id" => content_id, "user_id" => user_id, "reaction_type" => "fire" }) do
     Cache.remove_action(content_id, user_id)

     render(conn, "success.json", message: "success")
   end

   def create(_conn, _params), do: {:error, :invalid_inputs,  "Invalid params"}


   def show(conn, %{"content_id" => content_id}) do
    case Cache.get_content(content_id) do
       nil ->
        {:error, :not_found, "No such content was found"}

        content ->
          conn
          |> put_status(:ok)
          |> render("show.json", content: content)
    end
   end

end
