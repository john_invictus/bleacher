defmodule ContentTest do
  use ExUnit.Case, async: true


  alias Bleacher.Content.User
  alias Bleacher.Content

  test "Creating new content struct" do
    user = %User{id: 1}
    content = %Content{id: 1}

    assert content == Content.new(1)
    assert %Content{id: 1, fires: [user]} =  Content.new(content, user )

  end

  test "add_update_contents" do
    contents = 1..4
     |> Enum.map(fn id -> Content.new(Content.new(id), User.new(id)) end)


  # fire an existing topic but different user
  assert %Content{id: 1, fires: [User.new(2), User.new(1)]} == hd(Content.add_update_contents(contents, Content.new(1), User.new(2)))

  # fire to a valid topic but same user
  assert %Content{id: 1, fires: [User.new(1)]} == hd(Content.add_update_contents(contents, Content.new(1), User.new(1)))

  # fire a non existing topic
  assert %Content{id: 9, fires: [User.new(1)]} == hd(Content.add_update_contents(contents, Content.new(9), User.new(1)))


  #  add content to a new content
  assert %Content{id: 9, fires: [User.new(1)]} == hd(Content.add_update_contents([], Content.new(9), User.new(1)))
  end


  test "remove_update_contents" do
    contents = 1..4
    |> Enum.map(fn id -> Content.new(Content.new(id), User.new(id)) end)

    # remove a fire
    assert %Content{id: 1, fires: [], unfire: [%Bleacher.Content.User{id: 1}]} == hd(Content.remove_update_contents(contents, Content.new(1), User.new(1)))

    # remove fire from a non existent content
    assert %Content{count: 0, fires: [], id: 10, unfire: [%Bleacher.Content.User{id: 1}]} == hd(Content.remove_update_contents(contents, Content.new(10), User.new(1)))

    #remove from where user has not yet reacted
    assert [
        %Bleacher.Content{count: 0, fires: [%Bleacher.Content.User{id: 1}], id: 1, unfire: [%Bleacher.Content.User{id: 5}]},
        %Bleacher.Content{count: 0, fires: [%Bleacher.Content.User{id: 2}], id: 2, unfire: []},
        %Bleacher.Content{count: 0, fires: [%Bleacher.Content.User{id: 3}], id: 3, unfire: []},
        %Bleacher.Content{count: 0, fires: [%Bleacher.Content.User{id: 4}], id: 4, unfire: []}] == Content.remove_update_contents(contents, Content.new(1), User.new(5))
  end

  test "get_content" do
    contents = 1..4
    |> Enum.map(fn id -> Content.new(Content.new(id), User.new(id)) end)

    # fetch an existing value
    assert %Content{id: 1, fires: [User.new(1)], count: 1}  == Content.get_content(contents, 1)

    #fech non existing value
    assert nil  == Content.get_content(contents, 9)
  end
end
